#pragma once
#include <ttrapi/ttrapi.h>
#include <ttrapi/ttrgraph.h>
#include <iostream>

struct AiOptions
{
	std::string name;
	std::string	host;
	uint16_t	port;
};

class AiBase
{
public:
	AiBase(const AiOptions& _opt) : m_Opt(_opt) {}

	void execute()
	{
		try
		{
			m_Conn = std::make_shared<ttrapi::Connection>(m_Opt.name, m_Opt.host.c_str(), m_Opt.port);

			{
				ttrapi::Token token(m_Conn);
				thinkFirstRound(token);
			}

			while (true)
			{
				try {
					ttrapi::Token token(m_Conn);
					m_Map.updateOwner(m_Conn->gameState());
					think(token);
				}
				catch (ttrapi::timeout)
				{
					onTimeout();
				}
				//catch (ttrapi::rule_violation e)
				//{
				//	onRuleViolation(e);
				//}
			}
		}
		catch (ttrapi::connection_failure e)
		{
			onConnectionError(e);
		}
	}

protected:
	const ttrapi::GameState& state() const { return m_Conn->gameState(); }
	const ttrapi::Player& player() const { return m_Conn->playerState(); }


	virtual void think(ttrapi::Token& _token)
	{
		_token.drawCoverdTrainCard();
		_token.drawCoverdTrainCard();
	}

	virtual void thinkFirstRound(ttrapi::Token& _token)
	{
		_token.drawDestinationTickets();
	}

	virtual void onConnectionError(const ttrapi::connection_failure&)
	{

	}

	virtual void onTimeout()
	{}

	virtual void onRuleViolation(const ttrapi::rule_violation& e)
	{
		std::cerr << "rule violation " << e.what() << std::endl;
	}

	const AiOptions&					m_Opt;
	std::shared_ptr<ttrapi::Connection>	m_Conn;
	ttrapi::Map<>						m_Map;

};



