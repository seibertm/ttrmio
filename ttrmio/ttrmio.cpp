#include <ttrapi\ttrapi.h>
#include <thread>
#include <functional>
#include <vector>
#include <fstream>
#include "intrin.h"

#include "AiMiO.h"
#include "md5.h"


using namespace std;
using namespace ttrapi;

float nextByTimeStep(float _f)
{
	if (_f < 100) return _f += 0.00001f;
	if (_f < 1000) return _f += 0.0001f;
	return _f += 0.001f;
}

static int floatToChar(float _f, char* _out)
{
	const char* _fmt;
	if (_f < 100) _fmt = "%.5f";
	else if (_f < 1000) _fmt = "%.4f";
	else _fmt = "%.3f";

	int len = sprintf(_out, _fmt, _f);
	while (_out[len - 1] == '0') len--;
	return len;
}

void hashOfTimestamp(float _time, unsigned char* _hash)
{
	MD5_CTX ctx;
	MD5_Init(&ctx);

	char buffer[16];
	int len = floatToChar(_time, buffer);
	MD5_Update(&ctx, buffer, len);
	MD5_Final(_hash, &ctx);
}

void generateHashes()
{
	vector<pair<uint64_t, float> > hashLookup;
	hashLookup.reserve(1024 * 1024 * 64);

	for (float f = 10; f < 10000; f = nextByTimeStep(f))
	{
		MD5_CTX ctx;
		MD5_Init(&ctx);

		char buffer[16];
		int len = floatToChar(f, buffer);
		MD5_Update(&ctx, buffer, len);

		unsigned char result[16];
		MD5_Final(result, &ctx);

		uint64_t hash = *(uint64_t*)result;
		hash = _byteswap_uint64(hash);
		hashLookup.emplace_back(hash, f);
	}

	sort(hashLookup.begin(), hashLookup.end());
	ofstream idx("hash.idx", ofstream::trunc | ofstream::binary);
	idx.write((char*)hashLookup.data(), hashLookup.size() * sizeof(hashLookup.front()));
}



int main()
{
	//generateHashes();
	//return 0;

	AiOptions opt;
	opt.name = "MiO";
	opt.host = "127.0.0.1";
//	opt.host = "192.168.0.101";
//	opt.host = "192.168.0.101";
	opt.port = 13000;

//	AiMiO ai(opt);
//	ai.execute();
//	return 0;

	uint32_t seed = 1337;
	while (true)
	{
		GameServer srv;
		if (!srv.listen(opt.port))
			return 1;

		vector<thread> aiThreads;
		for (size_t i = 0; i < 3; ++i)
		{
			aiThreads.emplace_back(thread([&]() {
				AiMiO ai(opt);
				ai.execute();
			}));
			srv.waitForPlayer();
		}

//		srv.enableNetworkLogging("net.log");
		srv.state().shuffle(seed++);
		while (srv.run());

		for (int p : srv.points())
			cout << p << ' ';
		cout << endl;

		srv.close();
		for (auto& t : aiThreads)
			t.join();
	}


	return 0;
}
